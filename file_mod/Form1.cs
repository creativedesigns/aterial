﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using cdg_dm;
using file_mod;

namespace file_mod
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            if (!user.isDebug())
            {
                reservedToolStripMenuItem.Visible = false;
                reservedToolStripMenuItem.Enabled = false;
            }
        }

        private void fileInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 FileInfo = new Form2();
            FileInfo.MdiParent = this;
            FileInfo.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("File Info v1.0\nA tool to quickly display info read from a file\n\nCreative Designs\nDevModding.com");
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (global.fs != null) // empties the stream
                {
                    global.fs.Close();
                    global.br.Close();
                    global.bw.Close();
                }
                global.fs = new FileStream(@openFileDialog1.FileName, FileMode.Open, FileAccess.ReadWrite);
                global.br = new BinaryReader(global.fs);
                global.bw = new BinaryWriter(global.fs);
                global.meta = new metadata();
                global.vd = new volumedesc();
                toolStripStatusLabel2.Text = openFileDialog1.FileName;
            }
            else
                MessageBox.Show("File couldn't be opened :(");
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            global.fs.Close();
            toolStripStatusLabel2.Text = "None";
        }
    }
}
