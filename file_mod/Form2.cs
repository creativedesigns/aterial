﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using cdg_dm;
using cdg_debug;


namespace file_mod
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;
            textBox4.ReadOnly = true;
            textBox5.ReadOnly = true;
            textBox6.ReadOnly = true;
            textBox7.ReadOnly = true;
            textBox8.ReadOnly = true;
            textBox9.ReadOnly = true;
            textBox10.ReadOnly = true;
            textBox11.ReadOnly = true;
            textBox12.ReadOnly = true;
            textBox13.ReadOnly = true;
            textBox14.ReadOnly = true;
            textBox15.ReadOnly = true;
            textBox16.ReadOnly = true;
            textBox17.ReadOnly = true;
            textBox18.ReadOnly = true;
            textBox19.ReadOnly = true;
            textBox20.ReadOnly = true;
            textBox21.ReadOnly = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                global.meta.packagetype = STFSRead.readString_C(0, 4);
                textBox2.Text = global.meta.packagetype;
                textBox3.Text = STFSRead.detectContentType(global.meta.contype.ToString("X4"));
                textBox4.Text = global.meta.metaversion.ToString();
                textBox5.Text = global.meta.mediaid;
                textBox6.Text = global.meta.version.ToString("X");
                textBox7.Text = global.meta.bversion.ToString("X");
                textBox8.Text = global.meta.titleid;
                textBox9.Text = global.meta.conid;
                textBox10.Text = global.meta.profileid;
                textBox11.Text = global.meta.deviceid;
                textBox12.Text = global.meta.displayname;
                textBox13.Text = global.meta.titlename;
                textBox14.Text = STFSRead.detectTransferFlag(global.meta.tranferflag.ToString("X2"));
                textBox15.Text = global.vd.volmsize.ToString("X");
                textBox16.Text = global.vd.blocksep.ToString("X");
                textBox17.Text = global.vd.tablecount.ToString("X");
                textBox18.Text = global.vd.tablenum.ToString("X");
                textBox19.Text = global.vd.tophashtablesha.ToString("X");
                textBox20.Text = global.vd.allocatedblockcount.ToString("X");
                textBox21.Text = global.vd.unallocatedblockcount.ToString("X");
                pictureBox1.Image = global.meta.titleimg;
            }
            catch (Exception ex)
            {
                cdg_dm.cdg_dm.dbgOut("Caught exception: " + ex);
                pictureBox1.Image = file_mod.Properties.Resources.er64x64;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            // package type (0x0->0x3 length 4)
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            //content type (0x344->0x348 length 4)
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            //metadata version (0x348->0x34B length 4)
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            //Media ID (0x354->0x357 length 4)
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            //Version (0x358->0x35B lenght 4)
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            //Base Version (0x35C->0x35F length 4)
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            //title id (0x360->0x363 length 4)
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {
            //console id (0x36C->0x370 length 5)
        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {
            //profile id (0x371->0x378 length 8)
        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {
            //device id (0x3FD->0x410 length 0x14)
        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {
            //display name (0x411 length 0x18*12(0x900))
        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {
            //title name (0x1691 length 0x80)
        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {
            //transfer flags (0x1711 length 1)
        }

        private void textBox15_TextChanged(object sender, EventArgs e)
        {
            //block seperation
        }
    }
}
