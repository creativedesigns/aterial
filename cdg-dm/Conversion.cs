﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using cdg_debug;
using cdg_dm;

namespace cdg_dm
{
    public class changeval
    {
        public static ulong toULong(string s)
        {
            try
            {
                ulong UL = ulong.Parse(s, NumberStyles.HexNumber);
                cdg_dm.dbgOut("toULong: " + UL);
                return UL;
            }
            catch
            {
                return 0;
                cdg_dm.dbgOut("toULong: Error; " + s);
            }
        }

        public static long toLong(string s)
        {
            try
            {
                long L = long.Parse(s, NumberStyles.HexNumber);
                cdg_debug.cdg_debug.dbgOut("toLong: " + L);
                return L;
            }
            catch
            {
                return 0;
                cdg_dm.dbgOut("toLong: Error; " + s);
            }
        }
        public static int toInt(string s)
        {
            try
            {
                int i = int.Parse(s, NumberStyles.HexNumber);
                cdg_dm.dbgOut("toInt: " + i);
                return i;
            }
            catch
            {
                return 0;
                cdg_dm.dbgOut("toInt: Error; " + s);
            }
        }

        public static short toShort(string s)
        {
            try
            {
                short sh = short.Parse(s, NumberStyles.HexNumber);
                cdg_dm.dbgOut("toShort: " + sh);
                return sh;
            }
            catch
            {
                return 0;
                cdg_dm.dbgOut("toShort: Error; " + s);
            }
        }

        public static byte toByte(string s)
        {
            try
            {
                byte b = byte.Parse(s, NumberStyles.HexNumber);
                cdg_dm.dbgOut("toByte: " + b);
                return b;
            }
            catch
            {
                return 0;
                cdg_dm.dbgOut("toByte: Error; " + s);
            }
        }

        public static bool toBool(string s)
        {
            bool res;
            if (s.ToLower() == "true" || s == "1")
            {
                res = true;
                cdg_dm.dbgOut("toBool: " + res);
            }
            else if (s.ToLower() == "false" || s == "0")
            {
                res = false;
                cdg_dm.dbgOut("toBool: " + res);
            }
            else
                throw new Exception("Error, toBool cannot process string. Please make sure your string is the correct format!");
            return res;
        }

        public static string UniToASCII(string s)
        {
            byte[] unicodeb = Encoding.Unicode.GetBytes(s);
            byte[] asciib = Encoding.Convert(Encoding.Unicode, Encoding.ASCII, unicodeb);
            char[] reschar = new char[Encoding.ASCII.GetCharCount(asciib, 0, asciib.Length)];
            return new string(reschar);
        }
    }
}
