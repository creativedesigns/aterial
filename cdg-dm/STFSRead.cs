﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Drawing;
using System.Diagnostics;
using System.Globalization;
using cdg_dm;

namespace cdg_dm
{
    public class STFSRead
    {
        public static string readHex_A(long startpos, long length)
        {
            string hex = "";
            for (long i = startpos; i < startpos + length; i++)
            {
                global.fs.Position = i;
                byte[] buf = new byte[1];
                global.fs.Read(buf, 0, 1);
                hex += string.Format("{0:X}", buf[0]);
            }
            return hex;
        }

        public static string readString(long startpos, long length)
        {
            string c = "";
            for (long i = startpos; i < startpos + length; i = i + 2)
            {
                global.fs.Position = i;
                byte[] buf = new byte[1];
                global.fs.Read(buf, 0, 1);
                if (buf[0] == 0x00) // Null symbol, commonly found in Unicode strings but not whats needed here
                {
                }
                else
                {
                    char s = Convert.ToChar(buf[0]);
                    c += s;
                }
            }
            if (c == "" || c == " ")
            {
                c = "";
            }
            return c;
        }

        public static string readString_A(long startpos, long length)
        {
            string c = "";
            for (long i = startpos; i < startpos + length; i++)
            {
                global.fs.Position = i;
                byte[] buf = new byte[1];
                global.fs.Read(buf, 0, 1);
                char s = Convert.ToChar(buf[0]);
                c += s;
            }
            if (c == "." || c == " " || c.Length == 1 || c.Length == 0)
            {
                c = readString_A(startpos + 1, length);
            }
            return c;
        }

        public static string readString_C(long startpos, long length)
        {
            global.fs.Position = startpos;
            char[] readtext = global.br.ReadChars((int)length);
            string s = new string(readtext).Replace(".", "");
            return s;
        }

        public static Image loadThumbnailImage()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\aterial\\cache\\";
            string imgname = STFSRead.readHex_A(0x360, 0x4) + ".png";
            string dbg = "failed loading image "+imgname;
            if (Directory.Exists(@path))
            {
                if (File.Exists(@path + imgname))
                {
                        return Image.FromFile(@path + imgname);
                }
                else if (!File.Exists(@path + imgname))
                {
                    if (extractData(imgname, 0x171A, 0x4000))
                    {
                            return Image.FromFile(@path + imgname);
                    }
                    else
                        cdg_dm.dbgOut(dbg);
                }
                else
                    cdg_dm.dbgOut(dbg);
            }
            else if(!Directory.Exists(@path)){
                Directory.CreateDirectory(@path);
                if (File.Exists(@path + imgname))
                {
                        return Image.FromFile(path + imgname);
                    cdg_dm.dbgOut(dbg);
                }
                else if (!File.Exists(@path+imgname))
                {
                    if (extractData(imgname, 0x171A, 0x4000))
                    {
                            return Image.FromFile(path + imgname);
                    }
                    else
                        cdg_dm.dbgOut(dbg);
                }
                else
                    cdg_dm.dbgOut(dbg);
            }
            else
                cdg_dm.dbgOut(dbg);
            throw new Exception("Failed to load image");
        }

        public static Image loadThumbnailImage(string imgname)
        {
            Exception failed = new Exception("LoadImage failed. Contact a developer");
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)+"\\aterial\\cache\\";
            string dbg = "failed loading image " + imgname;
            if (Directory.Exists(@path))
            {
                if (File.Exists(@path + imgname))
                {
                    return Image.FromFile(@path + imgname);
                }
                else if (!File.Exists(@path))
                {
                    if (extractData(imgname, 0x171A, 0x4000))
                    {
                        return Image.FromFile(@path + imgname);
                    }
                    else
                        cdg_dm.dbgOut(dbg);
                }
                else
                    cdg_dm.dbgOut(dbg);
            }
            else if (!Directory.Exists(@path))
            {
                Directory.CreateDirectory(@path);
                if (File.Exists(@path + imgname))
                {
                    return Image.FromFile(@path + imgname);
                }
                else if (!File.Exists(@path))
                {
                    if (extractData(imgname, 0x171A, 0x4000))
                    {
                        return Image.FromFile(@path + imgname);
                    }
                    else
                        cdg_dm.dbgOut(dbg);
                }
                else
                    cdg_dm.dbgOut(dbg);
            }
            else
                cdg_dm.dbgOut(dbg);
            return cdg_dll.Resource1.er64x64;
        }

        public static Image loadImage(string imgname, long startpos, long length)
        {
            Exception failed = new Exception("LoadImage failed. Contact a developer");
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\aterial\\cache\\";
            string dbg = "failed loading image " + imgname;
            if (Directory.Exists(@path))
            {
                if (File.Exists(@path + imgname))
                {
                    return Image.FromFile(@path + imgname);
                }
                else if (!File.Exists(@path))
                {
                    if (extractData(imgname, startpos, length))
                    {
                        return Image.FromFile(@path + imgname);
                    }
                    else
                        cdg_dm.dbgOut(dbg);
                }
                else
                    cdg_dm.dbgOut(dbg);
            }
            else if (!Directory.Exists(@path))
            {
                Directory.CreateDirectory(@path);
                if (File.Exists(@path + imgname))
                {
                    return Image.FromFile(@path + imgname);
                }
                else if (!File.Exists(@path))
                {
                    if (extractData(imgname, startpos, length))
                    {
                        return Image.FromFile(@path + imgname);
                    }
                    else
                        cdg_dm.dbgOut(dbg);
                }
                else
                    cdg_dm.dbgOut(dbg);
            }
            else
                cdg_dm.dbgOut(dbg);
            return cdg_dll.Resource1.er64x64;
        }

        public static bool extractData(string filename, long startpos, long length)
        {
            Encoding encode = Encoding.GetEncoding("iso-8859-1");
            string data = STFSRead.readString_A(startpos, length);
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)+"\\aterial\\cache\\";
            cdg_dm.dbgOut("exctractData("+filename+", "+startpos.ToString("X")+", "+length.ToString("X")+"):");
            cdg_dm.dbgOut("Path: " + path + "\nFileName: " + filename);
            if (Directory.Exists(@path))
            {
                using (StreamWriter file = new StreamWriter(Path.Combine(path, filename), false, encode))
                {
                    file.Write(data);
                    file.Close();
                }
            }
            else if (!Directory.Exists(@path))
            {
                Directory.CreateDirectory(@path);
                using (StreamWriter file = new StreamWriter(Path.Combine(path, filename), false, encode))
                {
                    file.Write(data);
                    file.Close();
                }
            }
            if (File.Exists(Path.Combine(path, filename)))
                return true;
            else
            {
                cdg_dm.dbgOut("Couldn't extract file " + filename + " at location " + startpos.ToString("X") + " with a length of " + length);
                return false;
            }
        }

        public static bool extractData(long startpos, long length)
        {
            Encoding encode = Encoding.GetEncoding("iso-8859-1");
            string data = STFSRead.readString_A(startpos, length);
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\aterial\\cache\\";
            string filename = global.meta.titleid.Replace(" ", "") + ".png";
            if (Directory.Exists(@path))
            {
                using (StreamWriter file = new StreamWriter(@path + filename, false, encode))
                {
                    file.Write(data);
                    file.Close();
                }
            }
            else if (!Directory.Exists(@path))
            {
                using (StreamWriter file = new StreamWriter(@path + filename, false, encode))
                {
                    file.Write(data);
                    file.Close();
                }
            }
            if (File.Exists(@path + filename))
                return true;
            else
            {
                cdg_dm.dbgOut("Couldn't extract file " + filename + " at location " + startpos + " with a length of " + length);
                return false;
            }
        }

        public static string ToHex(char[] c)
        {
            string hexout = "";
            foreach (char _eachChar in c)
            {
                int value = Convert.ToInt32(_eachChar);
                hexout += string.Format("{0:X}", value);
            }
            return hexout;
        }

        public static string detectContentType(string val)
        {
            string contout;
            switch (val)
            {
                case "0001":
                    contout = "Saved Game";
                    break;
                case "0002":
                    contout = "Marketplace Content";
                    break;
                case "0003":
                    contout = "Publisher";
                    break;
                case "1000":
                    contout = "Xbox 360 Title";
                    break;
                case "4000":
                    contout = "Downloaded Game";
                    break;
                case "5000":
                    contout = "Xbox Title";
                    break;
                case "9000":
                    contout = "Avatar Item";
                    break;
                case "10000":
                    contout = "Profile";
                    break;
                case "20000":
                    contout = "Gamer Picture";
                    break;
                case "A0000":
                    contout = "Game Title";
                    break;
                case "C0000":
                    contout = "Game Trailer";
                    break;
                case "D0000":
                    contout = "Arcade Title";
                    break;
                case "E0000":
                    contout = "XNA Title";
                    break;
                default:
                    contout = "Unknown File";
                    break;
            }
            contout += " (" + val + ")";
            return contout;
        }

        public static string detectTransferFlag(string val)
        {
            string flagout;
            switch (val)
            {
                case "00":
                    flagout = "Device and Profile";
                    break;
                case "20":
                    flagout = "Move Only";
                    break;
                case "40":
                    flagout = "Device Only";
                    break;
                case "80":
                    flagout = "Profile Only";
                    break;
                case "C0":
                    flagout = "No Flags";
                    break;
                default:
                    flagout = "Unknown Flag";
                    break;
            }
            flagout += " (" + val + ")";
            return flagout;
        }

        public static string detectConsoleType(long con)
        {
            string contype;
            switch (con)
            {
                case 1:
                    contype = "Devkit";
                    break;
                case 2:
                    contype = "Retail";
                    break;
                default:
                    contype = "Unknown Console Type";
                    break;
            }
            contype += " (" + con + ")";
            return contype;
        }


        public static string getPackageType()
        {
            global.fs.Position = 0;
            string s = new string(global.br.ReadChars(4));
            if (s == "LIVE" || s == "PIRS")
            {
                return "Error";
            }
            return s;
        }

        public static string getContentType()
        {
            string content_t;
            content_t = readHex_A(0x344, 4);
            content_t = detectContentType(content_t);
            return content_t;
        }

        public static string getMetadataVersion()
        {
            return readHex_A(0x348, 4);
        }

        public static string getMediaID()
        {
            return readHex_A(0x354, 4);
        }

        public static string getVersion()
        {
            return readHex_A(0x358, 4);
        }

        public static string getBaseVersion()
        {
            return readHex_A(0x35C, 4);
        }

        public static string getTitleID()
        {
            return readHex_A(0x360, 4);
        }

        public static string getConsoleID()
        {
            return readHex_A(0x36C, 5);
        }

        public static string getProfileID()
        {
            return readHex_A(0x371, 8);
        }

        public static string getDeviceID()
        {
            return readHex_A(0x3FD, 0x14);
        }

        public static string getDisplayName()
        {
            return readString(0x412, 0x900);
        }

        public static string getTitleName()
        {
            return readString(0x1692, 0x80);
        }

        public static string getTransferFlags()
        {
            string flag = "";
            flag = detectTransferFlag(readHex_A(0x1711, 1));
            return flag;
        }
    }
}
