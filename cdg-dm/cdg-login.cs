﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;


/************************************************************************/
/*                                                                      */
/* TODO: Add Password Encryption Using PBKDF2                           */
/*                                                                      */
/************************************************************************/


namespace cdg_dm
{
    public static class user
    {
        private static string username = null;
        private static string password = null;
        private static bool loggedIn = true;
        private static bool online = false;
        private static bool dbgusr = false;
        private static bool developer = false;
        private static bool vip = false;
        private const bool AlphaBetaBuild = true;
        private const bool developmentBuild = true;
        private static bool bypass = true; // bypass being online for offline builds. remove from release build

        public static bool isDebug()
        {
            if (online || bypass)
                if (loggedIn)
                    if (dbgusr || developer || developmentBuild || AlphaBetaBuild)
                        return true;
                    else
                        return false;
                else
                    return false;
            else
                return false;
        }

        public static bool isVip()
        {
            if (online || bypass)
                if (loggedIn)
                    if (vip || dbgusr || developer || developmentBuild || AlphaBetaBuild)
                        return true;
                    else
                        return false;
                else
                    return false;
            else
                return false;
        }

        public static bool isOnline()
        {
            if (online || bypass)
                return true;
            else
                return false;
        }

        public static bool isLoggedIn()
        {
            if (loggedIn || bypass)
                return true;
            else
                return false;
        }

        public static string getUser()
        {
            if (loggedIn || developmentBuild || AlphaBetaBuild)
                return username;
            else
                return "Error";
        }
    }
}
