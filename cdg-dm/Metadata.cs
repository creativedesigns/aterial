﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace cdg_dm
{
    public class metadata
    {
        public metadata()
        {
            packagetype = STFSRead.readString_C(0, 4);
            pubkeysize = changeval.toLong(STFSRead.readHex_A(0x4, 2));
            ownerconid = changeval.toLong(STFSRead.readHex_A(0x6, 5));
            ownconpart = STFSRead.readString_C(0xB, 0x14);
            ownconsoletype = changeval.toByte(STFSRead.readHex_A(0x1F, 1));
            dateofgen = STFSRead.readString_C(0x20, 8);
            certsig = STFSRead.readHex_A(0xAC, 0x100);
            sig = STFSRead.readHex_A(0x1AC, 0x80);
            headersha = STFSRead.readHex_A(0x32C, 0x14);
            headersize = changeval.toLong(STFSRead.readHex_A(0x340, 0x4));
            contype = changeval.toLong(STFSRead.readHex_A(0x344, 0x4));
            metaversion = changeval.toLong(STFSRead.readHex_A(0x348, 0x4));
            consize = changeval.toLong(STFSRead.readHex_A(0x34C, 0x8));
            mediaid = STFSRead.readHex_A(0x354, 0x4);
            version = changeval.toLong(STFSRead.readHex_A(0x358, 0x4));
            bversion = changeval.toLong(STFSRead.readHex_A(0x35C, 0x4));
            titleid = STFSRead.readHex_A(0x360, 0x4);
            platform = changeval.toByte(STFSRead.readHex_A(0x364, 1));
            exetype = changeval.toByte(STFSRead.readHex_A(0x365, 1));
            discnum = changeval.toByte(STFSRead.readHex_A(0x366, 1));
            discinset = changeval.toByte(STFSRead.readHex_A(0x367, 1));
            saveid = STFSRead.readHex_A(0x368, 0x4);
            conid = STFSRead.readHex_A(0x36C, 0x5);
            profileid = STFSRead.readHex_A(0x371, 0x8);
            filecount = changeval.toLong(STFSRead.readHex_A(0x39D, 0x4));
            allfilesize = changeval.toLong(STFSRead.readHex_A(0x3A1, 0x8));
            desctype = changeval.toLong(STFSRead.readHex_A(0x3A9, 0x4));
            deviceid = STFSRead.readHex_A(0x3FD, 0x14);
            displayname = STFSRead.readString(0x411+1, 0x900);
            displaydesc = STFSRead.readString(0xD11+1, 0x900);
            pubname = STFSRead.readString(0x1611+1, 0x80);
            titlename = STFSRead.readString(0x1691+1, 0x80);
            tranferflag = changeval.toLong(STFSRead.readHex_A(0x1711, 1));
            thumbimgsize = changeval.toLong(STFSRead.readHex_A(0x1712, 0x4));
            titleimgsize = changeval.toLong(STFSRead.readHex_A(0x1716, 0x4));
            thumbimg = STFSRead.loadThumbnailImage(titleid + ".png");
            titleimg = STFSRead.loadImage(titleid + ".png", 0x571A, 0x4000);
            cdg_dm.dbgOut("PackageType: " + packagetype + "\nPublicKeySize: " + pubkeysize + "\nOwnerConsoleID: " + ownerconid + "\nOwnerConsolePart: " + ownconpart);
            cdg_dm.dbgOut("OwnerConsoleType: " + ownconsoletype + "\nDateOfGeneration: " + dateofgen);
            cdg_dm.dbgOut("Certificate Signature: " + certsig + "\nSignature: " + sig + "\nHeaderSha: " + headersha);
            cdg_dm.dbgOut("HeaderSize: " + headersize + "\nContentType: " + contype + "\nMetadataVersion: " + metaversion + "\nContentSize: " + consize + "\nMediaID: " + mediaid);
            cdg_dm.dbgOut("Version: " + version + "\nBaseVersion: " + bversion + "\nTitleID: " + titleid + "\nPlatform: " + platform);
            cdg_dm.dbgOut("ExecutableType: " + exetype);
            cdg_dm.dbgOut("DiscNumber: " + discnum + "\nDiscInSet: " + discinset + "\nSaveGameID: " + saveid + "\nConsoleID: " + conid + "\nProfileID: " + profileid);
            cdg_dm.dbgOut("FileCount: " + filecount + "\nAllFileSize: " + allfilesize + "\nDescriptor Type: " + desctype + "\nDeviceID: " + deviceid + "\nDisplayName: " + displayname);
            cdg_dm.dbgOut("DisplayDescription: " + displaydesc + "\nPublisherName: " + pubname + "\nTitleName: " + titlename + "\nTransferFlag: " + tranferflag);
            cdg_dm.dbgOut("ThumbNailImageSize: " + thumbimgsize + "\nTitleImageSize: " + titleimgsize);
        }
        public string packagetype;
        public long pubkeysize;
        public long ownerconid;
        public string ownconpart;
        public byte ownconsoletype;
        public string dateofgen;
        public string certsig; // using string for signatures.... can't find any type to contain a number thats 0x100 long
        public string sig;
        public string headersha;
        public long headersize;
        public long contype;
        public long metaversion;
        public long consize;
        public string mediaid;
        public long version;
        public long bversion;
        public string titleid;
        public byte platform;
        public byte exetype;
        public byte discnum;
        public byte discinset;
        public string saveid;
        public string conid;
        public string profileid;
        public long filecount;
        public long allfilesize;
        public long desctype;
        public string deviceid;
        public string displayname;
        public string displaydesc;
        public string pubname;
        public string titlename;
        public long tranferflag;
        public long thumbimgsize;
        public long titleimgsize;
        public Image thumbimg;
        public Image titleimg;
        public static metadata initmeta(FileStream fs, bool clear)
        {
            metadata x = new metadata();
            if (!clear)
            {
                x.packagetype = STFSRead.readString_C(0, 4);
                cdg_dm.dbgOut("x.package type: " + x.packagetype);
            }
            else
            {
                x.packagetype = "";
                x.pubkeysize = 0;
                x.ownerconid = 0;
                x.ownconpart = "";
                x.ownconsoletype = 0;
                x.dateofgen = "";
                x.certsig = "";
                x.sig = "";
                x.headersha = "";
                x.headersize = 0;
                x.contype = 0;
                x.metaversion = 0;
                x.consize = 0;
                x.mediaid = null;
                x.version = 0;
                x.bversion = 0;
                x.titleid = null;
                x.platform = 0;
                x.exetype = 0;
                x.discnum = 0;
                x.discinset = 0;
                x.saveid = null;
                x.conid = null;
                x.profileid = null;
                x.filecount = 0;
                x.allfilesize = 0;
                x.desctype = 0;
                x.deviceid = null;
                x.displayname = "";
                x.displaydesc = "";
                x.pubname = "";
                x.titlename = "";
                x.tranferflag = 0;
                x.thumbimgsize = 0;
                x.titleimgsize = 0;
                x.thumbimg = null;
                x.titleimg = null;
            }
            return x;
        }
    }
    public class volumedesc
    {
        public volumedesc()
        {
            volmsize = changeval.toByte(STFSRead.readHex_A(0x397, 0x01));
            blocksep = changeval.toByte(STFSRead.readHex_A(0x397+0x02, 0x01));
            tablecount = changeval.toLong(STFSRead.readHex_A(0x397+0x03, 0x02));
            tablenum = changeval.toLong(STFSRead.readHex_A(0x397+0x05, 0x03));
            tophashtablesha = changeval.toULong(STFSRead.readHex_A(0x397+0x08, 0x14));
            allocatedblockcount = changeval.toLong(STFSRead.readHex_A(0x397+0x1C, 0x04));
            unallocatedblockcount = changeval.toLong(STFSRead.readHex_A(0x397+0x20, 0x04));
            cdg_dm.dbgOut("VolumeSize: " + volmsize + "\nBlockSeperation: " + blocksep + "\nHashTableCount: " + tablecount);
            cdg_dm.dbgOut("HashTableNumber: " + tablenum + "\nTopHashTableHash: " + tophashtablesha + "\nAllocatedBlockCount: " + allocatedblockcount);
            cdg_dm.dbgOut("UnAllocatedBlockCount: " + unallocatedblockcount);
        }
        public byte volmsize;
        public byte blocksep;
        public long tablecount;
        public long tablenum;
        public ulong tophashtablesha;
        public long allocatedblockcount;
        public long unallocatedblockcount;
    }
}
