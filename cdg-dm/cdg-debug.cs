﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using cdg_dm;

/************************************************************************/
/*                  All Debug Related Stuff Goes Here                   */
/*    DO NOT EVER TRY TO CALL THE DEBUG CLASS OUT SIDE OF THIS FILE!    */
/* IF YOU WANT TO ACCESS SOMETHING WITHIN THE DEBUG CLASS MAKE A METHOD */
/*          IN THE cdg_dm CLASS BELOW IT LIKE THE dbgout METHOD         */
/************************************************************************/

namespace cdg_debug
{
    internal class cdg_debug
    {
        public static bool dbgOut(object dbg)
        {
            if (user.isDebug()) // Login system. Not yet implemented. Class found in cdg-Login
            {
                Debug.WriteLine(dbg);
                return true;
            }
            else
                return false;
        }
    }
}

namespace cdg_dm
{
    public class cdg_dm
    {
        public static bool dbgOut(object dbg)
        {
            if (cdg_debug.cdg_debug.dbgOut(dbg))
                return true;
            else
                return false;
        }
    }
}
